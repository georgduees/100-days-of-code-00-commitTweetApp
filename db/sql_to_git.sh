#!/bin/bash -e
# -e means exit if any command fails
DBHOST=127.0.0.1
DBUSER=dispatch
DBPASS=Dispatch123! # do this in a more secure fashion
DBNAME=dispatch
DBPORT=3306
GITREPO=../../100-days-of-code-00-commitTweetApp/db/
cd $GITREPO
mysqldump -h $DBHOST --port $DBPORT -u $DBUSER -p$DBPASS -d $DBNAME > $GITREPO/dispatch.sql # the -d flag means "no data"
git add dispatch.sql
git commit -m "$DBNAME schema version $(date)"
#git push # assuming you have a remote to push to