-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: dispatch
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB-10.2.13+maria~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `text` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `function_id` int(11) NOT NULL,
  `function_service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`user_id`,`function_id`,`function_service_id`),
  KEY `fk_activity_user1_idx` (`user_id`),
  KEY `fk_activity_function1_idx` (`function_id`,`function_service_id`),
  CONSTRAINT `fk_activity_function1` FOREIGN KEY (`function_id`, `function_service_id`) REFERENCES `function` (`id`, `service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authentication`
--

DROP TABLE IF EXISTS `authentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authentication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `url` varchar(2048) CHARACTER SET utf8 DEFAULT NULL,
  `type` int(11) DEFAULT 0,
  `secret` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `token` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authenticationservice`
--

DROP TABLE IF EXISTS `authenticationservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authenticationservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `service_id` int(11) NOT NULL,
  `authentication_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`service_id`,`authentication_id`),
  KEY `fk_authenticationservice_service1_idx` (`service_id`),
  KEY `fk_authenticationservice_authentication1_idx` (`authentication_id`),
  CONSTRAINT `fk_authenticationservice_authentication1` FOREIGN KEY (`authentication_id`) REFERENCES `authentication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_authenticationservice_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `useragentstring` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `uuid` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` int(11) DEFAULT 0,
  `data` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `function`
--

DROP TABLE IF EXISTS `function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`service_id`,`user_id`),
  KEY `fk_function_service1_idx` (`service_id`),
  KEY `fk_function_user1_idx` (`user_id`),
  CONSTRAINT `fk_function_service1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_function_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `image_url` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userauthentication`
--

DROP TABLE IF EXISTS `userauthentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userauthentication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `valid` tinyint(4) DEFAULT 0,
  `accesstoken` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `secret` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `timeout` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `authentication_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`user_id`,`authentication_id`),
  KEY `fk_userauthentication_user1_idx` (`user_id`),
  KEY `fk_userauthentication_authentication1_idx` (`authentication_id`),
  CONSTRAINT `fk_userauthentication_authentication1` FOREIGN KEY (`authentication_id`) REFERENCES `authentication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userauthentication_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userdevice`
--

DROP TABLE IF EXISTS `userdevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updatetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `lastused` timestamp NOT NULL DEFAULT current_timestamp(),
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `ip` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `longitude` decimal(10,0) DEFAULT NULL,
  `latitude` decimal(10,0) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`device_id`,`user_id`),
  KEY `fk_userdevice_device_idx` (`device_id`),
  KEY `fk_userdevice_user1_idx` (`user_id`),
  CONSTRAINT `fk_userdevice_device` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userdevice_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-17 10:42:09
