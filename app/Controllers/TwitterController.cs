﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace app.Controllers
{
    [Route("twitter")]
    public class TwitterController : Controller
    {
        private readonly IConfiguration configuration;
        public TwitterController(IConfiguration config)
        {
            this.configuration=config;
        }

                // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            
            var twitterDataList = configuration.GetSection("TwitterData");
            var responseString="09";
            responseString=twitterDataList.GetValue("AccessToken","none");

            return responseString;
        }

    }
}
