# 100-days-of-code-commitTweetApp
Commit Tweet app to tweet out my Commit-Messages to any related sub-repos via my twitter handle

# Log location:

https://github.com/georgduees/100-days-of-code/blob/master/log.md

# Sources / Inspiration Url's
- https://developer.github.com/webhooks/
- 

# ToDo
- [ ] Create WebAPI for User Interaction
- [ ] Create Webhook for GitHub
- [ ] Create OAUTH-Endpoints:
  - [ ] Twitter User Authentication
  - [ ] GitHub User Authentication
- [ ]- [ ] E-Mail Template for Logon
- [ ] E-Mail Template for Ö Create workflow for Password/Account-Login when Access is lost. (E-Mail-Link/Scan-QRCode 
with Mobile Device / Click Link in TwitterDM to Login)

//- [ ] Create DatabaseDesign

- [ ] Document Workflows
- [ ] Document Interaction
- [ ] Create Frontend (React?Angular?SIMPLEPLAIN HTML?PWA?)
    - [ ] Device Recognition
    - [ ] UserLogin
    - [ ] WelcomePage for Unknown Users
    - [ ] Authentication Dialogs
    - [ ] Administrative Dialogs
